from fastapi import FastAPI

app = FastAPI()

@app.get("/predict/{inputs}")
async def predict(inputs):
    return {"message": "Tomorrow is another day, {}!".format(inputs)}