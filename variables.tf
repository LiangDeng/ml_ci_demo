variable "public_ssh_key_path" {
  type    = string
  default = "./.ssh/my-key-pair.pem.pub"
}

variable "secret_ssh_key_path" {
  type    = string
  default = "./.ssh/my-key-pair.pem"
}
